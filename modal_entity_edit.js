(function($) {
  Drupal.behaviors.modal_entity_edit = {
    attach:function(context,settings){
      jQuery(".field-name-field-special", context).once("modal_entity_edit_form_dining_node_form", function(e){
				jQuery('.improvedselect li', this).each(function(index, element) {
          var so = jQuery(element).attr('so');
          jQuery(element).html('<div>' + jQuery(element).html() + '<br /><a href="/admin/node/edit/nojs/' + so + '" class="ctools-use-modal">Edit</a> | <a href="/admin/node/delete/nojs/' + so + '" class="ctools-use-modal">Delete</a></div>');
          // Now fire the behavior attach to catch the new ctools modal class.
          Drupal.attachBehaviors(element, Drupal.settings)
        });
        jQuery('label', this).append(' | <a href="/admin/node/add/nojs/special" class="ctools-use-modal">Add new Special</a>')
        Drupal.attachBehaviors(jQuery('label', this), Drupal.settings)
      });
    }
  }
}(jQuery));
